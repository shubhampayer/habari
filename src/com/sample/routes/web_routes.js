'use strict'

/**
* @class web_routes
* @extends com.sample.routes.base_routes
* @classdesc Application global app routes
* @memberof com.sample.routes
*/

const baseRoute = require("./base_routes.js").base_routes;
const path		= require("path");
	
/**
 * Export App Routes Class
 * @memberof com.sample.routes
 * @module api_routes
 * @see com.sample.routes.web_routes
 */

module.exports.web_routes = class web_routes extends baseRoute{
	
	constructor(app){
		super(app,__filename);
	}
	
	/**
	* @summary create static URLs
	* @public
	* @memberof com.sample.routes.web_routes
	* @function load
	* @override
	*/
	
	load(){
		this.app.get("/",this.controller.beforeLoad,this.controller.getHome);
		this.app.get("/the-power-of-connectivity",this.controller.beforeLoad,this.controller.getPowerOf);
		this.app.get("/the-year-of-return",this.controller.beforeLoad,this.controller.getYearOf);
		this.app.get("/five-books",this.controller.beforeLoad,this.controller.getFiveBooks);
		this.app.get("/end-of-year",this.controller.beforeLoad,this.controller.getEndYear);
		this.app.get("/mentor",this.controller.beforeLoad,this.controller.getMentor);
		this.app.get("/lifeAtAbsa",this.controller.beforeLoad,this.controller.getLifeAtAbsa);
	
	}
};
